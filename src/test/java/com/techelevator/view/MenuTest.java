package com.techelevator.view;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.techelevator.VendingMachine;
import com.techelevator.VendingMachineCLI;
import com.techelevator.view.Menu;

public class MenuTest {

	private ByteArrayOutputStream output;
	public VendingMachine coke;
	@Before
	public void setup() {
		output = new ByteArrayOutputStream();
		coke = new VendingMachine();
	}
	
	@Test
	public void displays_a_list_of_menu_options_and_prompts_user_to_make_a_choice() {
		Object[] options = new Object[] {  new Integer(3), "Blind", "Mice" };
		Menu menu = getMenuForTesting();
		
		menu.getChoiceFromOptions(options, 0);
		
		String expected = "\n"+
				 		  "1) "+options[0].toString()+"\n" + 
						  "2) "+options[1].toString()+"\n" +
						  "3) "+options[2].toString()+"\n\n" +
						  "Please choose an option >>> ";
		Assert.assertEquals(expected, output.toString());	  
	}
	
	@Test
	public void returns_object_corresponding_to_user_choice() {
		Integer expected = new Integer(456);
		Integer[] options = new Integer[] {  new Integer(123), expected, new Integer(789) };
		Menu menu = getMenuForTestingWithUserInput("2\n");

		Integer result = (Integer)menu.getChoiceFromOptions(options, 0);
		
		Assert.assertEquals(expected, result);	  
	}
	
	@Test
	public void redisplays_menu_if_user_does_not_choose_valid_option() {
		Object[] options = new Object[] {  "Larry", "Curly", "Moe" };
		Menu menu = getMenuForTestingWithUserInput("4\n1\n");
		
		menu.getChoiceFromOptions(options, 0);
		
		String menuDisplay = "\n"+
				 			 "1) "+options[0].toString()+"\n" + 
						     "2) "+options[1].toString()+"\n" +
						     "3) "+options[2].toString()+"\n\n" +
						     "Please choose an option >>> ";
		
		String expected = menuDisplay + 
					      "\n*** 4 is not a valid option ***\n\n" +
					      menuDisplay;
		
		Assert.assertEquals(expected, output.toString());
	}
	
	@Test
	public void redisplays_menu_if_user_chooses_option_less_than_1() {
		Object[] options = new Object[] {  "Larry", "Curly", "Moe" };
		Menu menu = getMenuForTestingWithUserInput("0\n1\n");
		
		menu.getChoiceFromOptions(options, 0);
		
		String menuDisplay = "\n"+
				 			 "1) "+options[0].toString()+"\n" + 
						     "2) "+options[1].toString()+"\n" +
						     "3) "+options[2].toString()+"\n\n" +
						     "Please choose an option >>> ";
		
		String expected = menuDisplay + 
					      "\n*** 0 is not a valid option ***\n\n" +
					      menuDisplay;
		
		Assert.assertEquals(expected, output.toString());
	}
	
	@Test
	public void redisplays_menu_if_user_enters_garbage() {
		Object[] options = new Object[] {  "Larry", "Curly", "Moe" };
		Menu menu = getMenuForTestingWithUserInput("Mickey Mouse\n1\n");
		
		menu.getChoiceFromOptions(options, 0);
		
		String menuDisplay = "\n"+
							 "1) "+options[0].toString()+"\n" + 
						     "2) "+options[1].toString()+"\n" +
						     "3) "+options[2].toString()+"\n\n" +
						     "Please choose an option >>> ";
		
		String expected = menuDisplay + 
					      "\n*** Mickey Mouse is not a valid option ***\n\n" +
					      menuDisplay;
		
		Assert.assertEquals(expected, output.toString());
	}
	
	@Test
	public void displays_a_list_of_process_menu_options_and_prompts_user_to_make_a_choice() {
		Object[] options = new Object[] {  new Integer(3), "Blind", "Mice" };
		Menu menu = getMenuForTesting();
		double zero = 0;
		
		menu.getChoiceFromProcessOptions(options, 0);
		
		String expected = "\n"+
				 		  "1) "+options[0].toString()+"\n" + 
						  "2) "+options[1].toString()+"\n" +
						  "3) "+options[2].toString()+"\n" +
						  String.format("%s%.2f", "Current Money Provided: $", zero) + "\n\n" +
						  "Please choose an option >>> ";
		Assert.assertEquals(expected, output.toString());	  
	}
	
	@Test
	public void redisplays_process_menu_if_user_enters_garbage() {
		Object[] options = new Object[] {  "Larry", "Curly", "Moe" };
		Menu menu = getMenuForTestingWithUserInput("Mickey Mouse\n1\n");
		double zero = 0;
		menu.getChoiceFromProcessOptions(options, 0);
		
		String menuDisplay = "\n"+
				 		     "1) "+options[0].toString()+"\n" + 
						     "2) "+options[1].toString()+"\n" +
						     "3) "+options[2].toString()+"\n" +
						      String.format("%s%.2f", "Current Money Provided: $", zero) + "\n\n" +
						      
						     "Please choose an option >>> ";
		
		String expected = menuDisplay + 
					      "\n*** Mickey Mouse is not a valid option ***\n\n" +
					      menuDisplay;
		
		Assert.assertEquals(expected, output.toString());
	}
	
	@Test
	public void redisplays_process_menu_if_user_enters_number_less_than_one() {
		Object[] options = new Object[] {  "Larry", "Curly", "Moe" };
		Menu menu = getMenuForTestingWithUserInput("0\n1\n");
		double zero = 0;
		menu.getChoiceFromProcessOptions(options, 0);
		
		String menuDisplay = "\n"+
				 		     "1) "+options[0].toString()+"\n" + 
						     "2) "+options[1].toString()+"\n" +
						     "3) "+options[2].toString()+"\n" +
						      String.format("%s%.2f", "Current Money Provided: $", zero) + "\n\n" +
						      
						     "Please choose an option >>> ";
		
		String expected = menuDisplay + 
					      "\n*** 0 is not a valid option ***\n\n" +
					      menuDisplay;
		
		Assert.assertEquals(expected, output.toString());
	}
	
	@Test
	public void redisplays_process_menu_if_user_enters_number_greater_than_three() {
		Object[] options = new Object[] {  "Larry", "Curly", "Moe" };
		Menu menu = getMenuForTestingWithUserInput("4\n1\n");
		double zero = 0;
		menu.getChoiceFromProcessOptions(options, 0);
		
		String menuDisplay = "\n"+
				 		     "1) "+options[0].toString()+"\n" + 
						     "2) "+options[1].toString()+"\n" +
						     "3) "+options[2].toString()+"\n" +
						      String.format("%s%.2f", "Current Money Provided: $", zero) + "\n\n" +
						      
						     "Please choose an option >>> ";
		
		String expected = menuDisplay + 
					      "\n*** 4 is not a valid option ***\n\n" +
					      menuDisplay;
		
		Assert.assertEquals(expected, output.toString());
	}
	
	@Test
	public void getDollarAmount_returns_dollar_amount() {
		Menu menu = getMenuForTestingWithUserInput("10");
		String moneyFed = menu.getDollarAmountFromUser();
		Assert.assertEquals("Should return 10 if user inputs 10", "10", moneyFed);
	}
	
	@Test
	public void getProductKey_returns_key() {
		Menu menu = getMenuForTestingWithUserInput("A1");
		String key = menu.getProductKey();
		Assert.assertEquals("Should return A1 if user inputs A1", "A1", key);
	}
	
	@Test
	public void getAudit_returns_purchased_item() {
		Menu menu = getMenuForTestingWithUserInput("A1");
		String key = menu.getProductKey();
		Assert.assertEquals("Should return A1 if user inputs A1", "A1", key);
	}
	
	@Test
	public void getFeedMoney_returns_feed_money_for_$5() {
		coke.feedMoney("5");
		double money = coke.getFeedMoney();
		Assert.assertEquals("Should return 5 feed money", 5, money, 0);
	}
	
	@Test
	public void getFeedMoney_returns_0_money_for_invalid_entry() {
		coke.feedMoney("7");
		double money = coke.getFeedMoney();
		Assert.assertEquals("Should return 0 feed money", 0, money, 0);
	}
	
	@Test
	public void getFeedMoney_returns_feed_money_for_$1() {
		coke.feedMoney("1");
		double money = coke.getFeedMoney();
		Assert.assertEquals("Should return 1 feed money", 1, money, 0);
	}
	
	@Test
	public void getFeedMoney_returns_feed_money_for_$10() {
		coke.feedMoney("10");
		double money = coke.getFeedMoney();
		Assert.assertEquals("Should return 10 feed money", 10, money, 0);
	}
	
	@Test
	public void getFeedMoney_returns_feed_money_for_$20() {
		coke.feedMoney("20");
		double money = coke.getFeedMoney();
		Assert.assertEquals("Should return 20 feed money", 20, money, 0);
	}
	
	@Test
	public void getFeedMoney_returns_feed_money_for_$2() {
		coke.feedMoney("2");
		double money = coke.getFeedMoney();
		Assert.assertEquals("Should return 2 feed money", 2, money, 0);
	}
	
	@Test
	public void getFeedMoney_returns_feed_money_for_garbage() {
		coke.feedMoney("Mickey Mouse");
		double money = coke.getFeedMoney();
		Assert.assertEquals("Should return 0 feed money for invalid entry", 0, money, 0);
	}
	
	@Test
	public void selectProduct_returns_purchased_item() {
		coke.feedMoney("2");
		coke.selectProduct("D1", coke);
		double money = coke.getFeedMoney();
		List<String> items = new ArrayList<String>(coke.getPurchasedItems());
		List<String> expectedItems = new ArrayList<String>();
		expectedItems.add("U-Chews");
		Assert.assertEquals("Should return 1.15 feed money", 1.15, money, 0);
		Assert.assertEquals("Should return U-Chews", expectedItems, items);
	}
	
	@Test
	public void selectProduct_does_not_respond_to_invalid_entry() {
		coke.feedMoney("2");
		coke.selectProduct("Mickey Mouse", coke);
		double money = coke.getFeedMoney();
		List<String> items = new ArrayList<String>(coke.getPurchasedItems());
		List<String> expectedItems = new ArrayList<String>();
		Assert.assertEquals("Should return 2 feed money", 2, money, 0);
		Assert.assertEquals("Should return empty array for invalid entry", expectedItems, items);
	}
	
	@Test
	public void selectProduct_does_not_respond_if_insufficient_funds() {
		coke.feedMoney("1");
		coke.selectProduct("A1", coke);
		double money = coke.getFeedMoney();
		List<String> items = new ArrayList<String>(coke.getPurchasedItems());
		List<String> expectedItems = new ArrayList<String>();
		Assert.assertEquals("Should return 1 feed money", 1, money, 0);
		Assert.assertEquals("Should return empty array for invalid entry", expectedItems, items);
	}
	
	@Test
	public void selectProduct_does_not_respond_if_sold_out() {
		coke.feedMoney("20");
		coke.selectProduct("A1", coke);
		coke.selectProduct("A1", coke);
		coke.selectProduct("A1", coke);
		coke.selectProduct("A1", coke);
		coke.selectProduct("A1", coke);
		coke.selectProduct("A1", coke);
		coke.selectProduct("A1", coke);
		double money = coke.getFeedMoney();
		List<String> items = new ArrayList<String>(coke.getPurchasedItems());
		List<String> expectedItems = new ArrayList<String>();
		expectedItems.add("Potato Crisps");
		expectedItems.add("Potato Crisps");
		expectedItems.add("Potato Crisps");
		expectedItems.add("Potato Crisps");
		expectedItems.add("Potato Crisps");
		Assert.assertEquals("Should return 4.75 feed money", 4.75, money, 0.01);
		Assert.assertEquals("Should return empty array for invalid entry", expectedItems, items);
	}
	
	@Test
	public void getSounds_return_proper_sounds() {
		coke.feedMoney("20");
		coke.selectProduct("A1", coke);
		coke.selectProduct("B1", coke);
		coke.selectProduct("C1", coke);
		coke.selectProduct("D1", coke);
		List<String> sounds = new ArrayList<String>();
		sounds = coke.getSounds();
		
		List<String> expectedSounds = new ArrayList<String>();
		expectedSounds.add("Crunch Crunch, Yum!");
		expectedSounds.add("Munch Munch, Yum!");
		expectedSounds.add("Glug Glug, Yum!");
		expectedSounds.add("Chew Chew, Yum!");
		
		Assert.assertEquals("Should return all yum sounds", expectedSounds, sounds);
	}
	
	@Test
	public void getSounds_returns_no_sounds() {
		coke.feedMoney("20");
		List<String> sounds = new ArrayList<String>();
		sounds = coke.getSounds();
		
		List<String> expectedSounds = new ArrayList<String>();
		
		Assert.assertEquals("Should return no no yum sounds", expectedSounds, sounds);
	}
	
	@Test
	public void getSounds_returns_gum_sounds() {
		coke.feedMoney("20");
		List<String> sounds = new ArrayList<String>();
		coke.selectProduct("D1", coke);
		sounds = coke.getSounds();
		
		List<String> expectedSounds = new ArrayList<String>();
		expectedSounds.add("Chew Chew, Yum!");
		
		Assert.assertEquals("Should return gum yum sounds", expectedSounds, sounds);
	}

	@Test
	public void changeBack_returns_correct_change() {
		coke.feedMoney("2");
		coke.selectProduct("D1", coke);
		int quarters = 4;
		int dimes = 1;
		int nickels = 1;
		String change = coke.changeBack();
		String expectedChange = "Change " + "$1.15" + "\nQuarters: " + quarters + "\nDimes: " + dimes + "\nNickels: " + nickels;
		Assert.assertEquals("Should return 4 quarters, one dime and one nickel", expectedChange, change);
	}
	
//	@Test
//	public void menuDisplays_if_user_selects_1_from_display_menu() {
//		
//		Menu menu = getMenuForTestingWithUserInput("1");
//		VendingMachineCLI cli = new VendingMachineCLI(menu);
//		
//		String display = "\n" + "A1| Potato Crisps| $3.05\n" + 
//							   "A2| Stackers| $1.75\n" + 
//							   "A3| Grain Waves| $2.75\n" + 
//							   "A4| Cloud Popcorn| $3.65\n" +
//							   "B1| Moonpie| $1.80\n" +
//							   "B2| Cowtales| $1.50\n" +
//							   "B3| Wonka Bar| $1.50\n" +
//							   "B4| Crunchie| $1.75\n" +
//							   "C1| Cola| $1.25\n" +
//							   "C2| Dr. Salt| $1.50\n" +
//							   "C3| Mountain Melter| $1.50\n" +
//							   "C4| Heavy| $1.50\n" +
//							   "D1| U-Chews| $0.85\n" +
//							   "D2| Little League Chew| $0.95\n" +
//							   "D3| Chiclets| $0.75\n" +
//							   "D4| Triplemint| $0.75\n\n\n" +
//							   "1) Display Vending Machine Items\n" + 
//							   "2) Purchase\n\n" +
//							   "Please choose an option >>> ";
//		
//		Assert.assertEquals("Should return display menu", display, output.toString());
//							   
//	}
	
	@Test
	public void cli_runs_properly() {
		Menu menu = new Menu(System.in, System.out);
		VendingMachineCLI cli = new VendingMachineCLI(menu);
		cli.run();
		String display = "1) Display Vending Machine Items\n" + 
				   		 "2) Purchase\n\n" +
				   		 "Please choose an option >>> ";
		Assert.assertEquals("System display should prompt user correctly", display, output.toString());
	}
	
	private Menu getMenuForTestingWithUserInput(String userInput) {
		ByteArrayInputStream input = new ByteArrayInputStream(String.valueOf(userInput).getBytes());
		return new Menu(input, output);
	}
	
	private Menu getMenuForTesting() {
		return getMenuForTestingWithUserInput("1\n");
	}
	
}
