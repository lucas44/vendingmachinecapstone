package com.techelevator.view;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.nio.file.NoSuchFileException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Scanner;

import com.techelevator.VendingMachine;



public class Menu {

	private PrintWriter out;
	private Scanner in;
	private List <String> audit = new ArrayList<String>();
	public Map <String, Integer> salesReport = new HashMap<String, Integer>();

	public Menu(InputStream input, OutputStream output) {
		this.out = new PrintWriter(output);
		this.in = new Scanner(input);
	}

	public Object getChoiceFromOptions(Object[] options, double balance) {
		Object choice = null;
		while(choice == null) {
			displayMenuOptions(options, balance);
			choice = getChoiceFromUserInput(options);
		}
		return choice;
	}
	
	public Object getChoiceFromProcessOptions(Object[] options, double balance) {
		Object choice = null;
		while(choice == null) {
			displayProcessMenu(options, balance);
			choice = getProcessChoiceFromUserInput(options);
		}
		return choice;
	}
	
	public String getDollarAmountFromUser() {
		String dollarAmount = "";
		dollarAmount = in.nextLine();
		return dollarAmount;
	}
	
	public String getProductKey() {
		System.out.println();
		String key = in.nextLine();
		return key.toUpperCase();
	}

	private Object getChoiceFromUserInput(Object[] options) {
		Object choice = null;
		String userInput = in.nextLine();
		try {
			int selectedOption = Integer.valueOf(userInput);
			if(selectedOption > 0 && selectedOption <= 2) {
				choice = options[selectedOption - 1];
			}
		} catch(NumberFormatException e) {
			// eat the exception, an error message will be displayed below since choice will be null
		}
		if(choice == null) {
			out.println("\n*** "+userInput+" is not a valid option ***\n");
		}
		return choice;
	}
	
	private Object getProcessChoiceFromUserInput(Object[] options) {
		Object choice = null;
		String userInput = in.nextLine();
		try {
			int selectedOption = Integer.valueOf(userInput);
			if(selectedOption > 0 && selectedOption <= 3) {
				choice = options[selectedOption - 1];
			}
		} catch(NumberFormatException e) {
			// eat the exception, an error message will be displayed below since choice will be null
		}
		if(choice == null) {
			out.println("\n*** "+userInput+" is not a valid option ***\n");
		}
		return choice;
	}

	private void displayMenuOptions(Object[] options, double balance) {
		out.println();
		for(int i = 0; i < options.length; i++) {
			int optionNum = i+1;
			out.println(optionNum+") "+options[i]);
		}
		out.print("\nPlease choose an option >>> ");
		out.flush();
	}
	
	private void displayProcessMenu(Object[] options, double balance) {
		out.println();
		for (int i = 0; i < options.length; i++) {
			int optionNum = i+1;
			out.println(optionNum+") "+options[i]);
		}
		out.println(String.format("%s%.2f", "Current Money Provided: $", balance));
		out.print("\nPlease choose an option >>> ");
		out.flush();
	}
	public void logEntry(String log) {
		audit.add(log);
	}
	public void printAudit() {
		try (PrintWriter fileOutput = new PrintWriter("Log.txt")){
			for (String entry : audit) {
				fileOutput.println(entry);
			}
		} catch (FileNotFoundException e) {
			out.println("File not found");
		}
	}
	
	public List<String> getAudit(){
		return audit;
	}
	
	public List<String> printReport(VendingMachine machine,  Map salesReport){
		VendingMachine coke = new VendingMachine();
		coke = machine;
//		Map<String, Integer> productQuantity = new HashMap<String, Integer>();
		Map<String, String> productNames = new HashMap<String, String>();
		List<String> productNamesList = new ArrayList<String>();
		
//		productQuantity = coke.getProductMenuQuantity();
		productNames = coke.getProductMenu();
		
		for (int i = 0; i < 4; i++) {
			productNamesList.add(productNames.get("A" + (i+1)));
		}
		
		for (int i = 0; i < 4; i++) {
			productNamesList.add(productNames.get("B" + (i+1)));
		}
		
		for (int i = 0; i < 4; i++) {
			productNamesList.add(productNames.get("C" + (i+1)));
		}
		
		for (int i = 0; i < 4; i++) {
			productNamesList.add(productNames.get("D" + (i+1)));
		}
		
		try (PrintWriter fileOutput = new PrintWriter(new FileWriter("Sales_Report.txt"))) {
			for (String item : productNamesList) {
				if (!salesReport.get(item).equals(null)) {
					fileOutput.println(item + "|" + salesReport.get(item));
				} else {
				fileOutput.println(item + "|" + 0);
				}
				
			}
			fileOutput.println();
			fileOutput.println("**TOTALSALES** $" + String.format("%.2f", getTotalSales()));
		} catch (Exception e) {
			System.out.println("It didn't work");
		}
		
		return audit;
	}
	private double getTotalSales() {
		double total = 0;

		total = total + salesReport.get("Potato Crisps") * 3.05;
		total = total + salesReport.get("Stackers") * 1.75;
		total = total + salesReport.get("Grain Waves") * 2.75;
		total = total + salesReport.get("Cloud Popcorn") * 3.65;
		total = total + salesReport.get("Moonpie") * 1.80;
		total = total + salesReport.get("Cowtales") * 1.50;
		total = total + salesReport.get("Wonka Bar") * 1.50;
		total = total + salesReport.get("Crunchie") * 1.75;
		total = total + salesReport.get("Cola") * 1.25;
		total = total + salesReport.get("Dr. Salt") * 1.50;
		total = total + salesReport.get("Mountain Melter") * 1.50;
		total = total + salesReport.get("Heavy") * 1.50;
		total = total + salesReport.get("U-Chews") * .85;
		total = total + salesReport.get("Little League Chew") * .95;
		total = total + salesReport.get("Chiclets") * .75;
		total = total + salesReport.get("Triplemint") * .75;
		return total;
	}

	public boolean checkForFile(String fileName) {
		File file = new File(fileName);
		if (file.exists()) {
			return true;
		} else {
			return false;
		}
		
	}

	public Map<String, Integer> getSalesReport() {
		return salesReport;
	}

	public void setSalesReport(Map<String, Integer> salesReport) {
		this.salesReport = salesReport;
	}

	public Map<String, Integer> readSalesReport() {
		File salesReportFile = new File("Sales_Report.txt");
		Map <String, Integer> list = new HashMap<String, Integer>();
		try (Scanner systemReader = new Scanner(salesReportFile)) {
			for (int i=0 ; i < 16 ; i++) {
				String[] product = systemReader.nextLine().split("\\|");
				list.put(product[0], Integer.parseInt(product[1]));
			}
		} catch (FileNotFoundException e) {
			VendingMachine machine = new VendingMachine();
			for (int i = 0; i < 4; i++) {
				list.put(machine.getProductMenu().get("A" + (i+1)), 0);
			}
			
			for (int i = 0; i < 4; i++) {
				list.put(machine.getProductMenu().get(("B" + (i+1))), 0);
			}
			
			for (int i = 0; i < 4; i++) {
				list.put(machine.getProductMenu().get(("C" + (i+1))), 0);
			}
			
			for (int i = 0; i < 4; i++) {
				list.put(machine.getProductMenu().get(("D" + (i+1))), 0);
			}
		} setSalesReport(list);
		  return list;
		
	}
}
