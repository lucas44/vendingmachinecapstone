package com.techelevator;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class VendingMachine {
	private double feedMoney = 0;
	
	public Map <String, String> productMenu = new HashMap<String, String>();
	public Map <String, Double> productMenuCost = new HashMap<String, Double>();
	public Map <String, Integer> productMenuQuantity = new HashMap<String, Integer>();
	public List <String> itemsPurchased = new ArrayList<String>();
	
	public VendingMachine() {
		productMenu.put("A1", "Potato Crisps");
		productMenu.put("A2", "Stackers");
		productMenu.put("A3", "Grain Waves");
		productMenu.put("A4", "Cloud Popcorn");
		productMenu.put("B1", "Moonpie");
		productMenu.put("B2", "Cowtales");
		productMenu.put("B3", "Wonka Bar");
		productMenu.put("B4", "Crunchie");
		productMenu.put("C1", "Cola");
		productMenu.put("C2", "Dr. Salt");
		productMenu.put("C3", "Mountain Melter");
		productMenu.put("C4", "Heavy");
		productMenu.put("D1", "U-Chews");
		productMenu.put("D2", "Little League Chew");
		productMenu.put("D3", "Chiclets");
		productMenu.put("D4", "Triplemint");
		
		productMenuCost.put("A1", 3.05);
		productMenuCost.put("A2", 1.75);
		productMenuCost.put("A3", 2.75);
		productMenuCost.put("A4", 3.65);
		productMenuCost.put("B1", 1.80);
		productMenuCost.put("B2", 1.50);
		productMenuCost.put("B3", 1.50);
		productMenuCost.put("B4", 1.75);
		productMenuCost.put("C1", 1.25);
		productMenuCost.put("C2", 1.50);
		productMenuCost.put("C3", 1.50);
		productMenuCost.put("C4", 1.50);
		productMenuCost.put("D1", .85);
		productMenuCost.put("D2", .95);
		productMenuCost.put("D3", .75);
		productMenuCost.put("D4", .75);
		
		productMenuQuantity.put("A1", 5);
		productMenuQuantity.put("A2", 5);
		productMenuQuantity.put("A3", 5);
		productMenuQuantity.put("A4", 5);
		productMenuQuantity.put("B1", 5);
		productMenuQuantity.put("B2", 5);
		productMenuQuantity.put("B3", 5);
		productMenuQuantity.put("B4", 5);
		productMenuQuantity.put("C1", 5);
		productMenuQuantity.put("C2", 5);
		productMenuQuantity.put("C3", 5);
		productMenuQuantity.put("C4", 5);
		productMenuQuantity.put("D1", 5);
		productMenuQuantity.put("D2", 5);
		productMenuQuantity.put("D3", 5);
		productMenuQuantity.put("D4", 5);
	}
	
	public double getFeedMoney() {
		return feedMoney;
	}

	public void setFeedMoney(double moneyFed) {
		this.feedMoney = moneyFed;
	}
	
	public void purchaseItem(String itemPurchased) {
		this.itemsPurchased.add(itemPurchased);
	}
	public List<String> getPurchasedItems() {
		return itemsPurchased;
	}
	
	public String getSalesReport(String key) {
		return productMenu.get(key.toUpperCase()) + "|" + (5 - productMenuQuantity.get(key.toUpperCase()));
	}
	
	public void feedMoney(String dollarAmount) {
		System.out.println();	 
		double moneyFed = 0;
		try {
			moneyFed = Integer.valueOf(dollarAmount);
		} catch (NumberFormatException e) {
			System.out.println(dollarAmount + " is an invalid entry.");
		}
			if (moneyFed == 20) {
				setFeedMoney(feedMoney + 20);
			} else if (moneyFed == 10) {
				setFeedMoney(feedMoney + 10);
			} else if (moneyFed == 5) {
				setFeedMoney(feedMoney + 5);
			} else if (moneyFed == 2) {
				setFeedMoney(feedMoney + 2);
			}else if (moneyFed == 1) {
				setFeedMoney(feedMoney + 1);
			} else if (moneyFed > 0){
				System.out.println(String.format("%s%.2f", "$",moneyFed) + " is an invalid amount");
			}
	}
	
	public void selectProduct(String key, VendingMachine machine) {
		VendingMachine coke = machine;
		double cost;
		int quantity;
		String item;
		try {
			cost = coke.getProductMenuCost().get(key.toUpperCase());
			quantity = coke.getProductMenuQuantity().get(key.toUpperCase());
			item = coke.getProductMenu().get(key.toUpperCase());
			
			if (cost > coke.getFeedMoney()) {
				System.out.println("Insufficient funds, please feed money");
			} else if (quantity > 0) {
				quantity--;
				coke.productMenuQuantity.put(key.toUpperCase(), quantity);
				coke.purchaseItem(item);
				System.out.println("You purchase a " + coke.productMenu.get(key.toUpperCase()) + "!");
				System.out.println("Items purchased: ");
				for (String purchase : coke.getPurchasedItems()) {
					System.out.println(purchase);
				}
				coke.setFeedMoney(coke.getFeedMoney() - cost);
			} else {
				System.out.println("Item is sold out!");
			}
		} catch (NullPointerException e) {
			System.out.println("\n*** "+key.toUpperCase()+" is not a valid option ***\n");
		}
	}

	public Map<String, String> getProductMenu() {
		return productMenu;
	}

	public Map<String, Double> getProductMenuCost() {
		return productMenuCost;
	}

	public Map<String, Integer> getProductMenuQuantity() {
		return productMenuQuantity;
	}
	public String chipSound() {
		return "Crunch Crunch, Yum!";
	}
	public String candySound() {
		return "Munch Munch, Yum!";
	}
	public String drinkSound() {
		return "Glug Glug, Yum!";
	}
	public String gumSound() {
		return "Chew Chew, Yum!";
	}
	public List<String> getSounds() {
		List <String> sounds = new ArrayList <String>();
		
		List <String> chip = new ArrayList <String>(); 
		for (int i = 1; i < 5; i++) {
		chip.add(productMenu.get("A" + i));
		}
		List <String> candy = new ArrayList <String>(); 
		for (int i = 0; i < 5; i++) {
			candy.add(productMenu.get("B" + i));
			}
		List <String> drinks = new ArrayList <String>();
		for (int i = 0; i < 5; i++) {
			drinks.add(productMenu.get("C" + i));
			}
		List <String> gum = new ArrayList <String>();
		for (int i = 0; i < 5; i++) {
			gum.add(productMenu.get("D" + i));
			}
		for (String item : itemsPurchased) {
			if (chip.contains(item)) {
				sounds.add(chipSound());
			} else if (candy.contains(item)) {
				sounds.add(candySound());
			} else if (drinks.contains(item)) {
				sounds.add(drinkSound());
			} else if (gum.contains(item)) {
				sounds.add(gumSound());
			}
		}
		return sounds;
	}
	
	public String changeBack() {
		double change = getFeedMoney();
		int quarters = 0;
		int dimes = 0;
		int nickels = 0;
		while (change >= .22) {
			quarters++;
			change -= .25;
		}
		while (change >= .07) {
			dimes++;
			change -= .1;
		}
		while (change > .03) {
			nickels++;
			change -= .05;
		}
		return "Change $" + String.format("%.2f", getFeedMoney()) + "\nQuarters: " + quarters + "\nDimes: " + dimes + "\nNickels: " + nickels;
	}

	public boolean isProduct(String key) {
		try {
			
			if (!productMenu.get(key.toUpperCase()).isEmpty());
				return true;
		} catch (NullPointerException e){
			return false;
		}
	}
	public String getProduct(String key) {
		return productMenu.get(key.toUpperCase());
	}
}
