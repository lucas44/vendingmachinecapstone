package com.techelevator;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.techelevator.view.Menu;

public class VendingMachineCLI {

	private static final String MAIN_MENU_OPTION_DISPLAY_ITEMS = "Display Vending Machine Items";
	private static final String MAIN_MENU_OPTION_PURCHASE = "Purchase";
	private static final String MAIN_MENU_OPTION_FEED_MONEY = "Feed Money";
	private static final String MAIN_MENU_OPTION_SELECT_PRODUCT = "Select Product";
	private static final String MAIN_MENU_OPTION_FINISH_TRANSACTION = "Finish Transaction";
	private static final String[] MAIN_MENU_OPTIONS = { MAIN_MENU_OPTION_DISPLAY_ITEMS,
													   MAIN_MENU_OPTION_PURCHASE};
	private static final String[] PROCESS_MENU_OPTIONS = {MAIN_MENU_OPTION_FEED_MONEY, 
														MAIN_MENU_OPTION_SELECT_PRODUCT, 
														MAIN_MENU_OPTION_FINISH_TRANSACTION};
	public static Map <String, Integer> salesReport = new HashMap<String, Integer>();
	
	private Menu menu;
	
	public VendingMachineCLI(Menu menu) {
		this.menu = menu;
	}
	
	public void run() {
		VendingMachine coke;
		coke = new VendingMachine();
		Date time = new Date();
		Map <String, Integer> salesReport = new HashMap < String, Integer>();
		salesReport = menu.readSalesReport();
		boolean finished = false;
//		System.out.println(salesReport);
		
		
		while(!finished) {
			String choice = null;
			choice = (String)menu.getChoiceFromOptions(MAIN_MENU_OPTIONS, coke.getFeedMoney());
			
			if(choice.equals(MAIN_MENU_OPTION_DISPLAY_ITEMS)) {
				
				System.out.println();
				System.out.println("A1| Potato Crisps| $3.05");
				System.out.println("A2| Stackers| $1.75");
				System.out.println("A3| Grain Waves| $2.75");
				System.out.println("A4| Cloud Popcorn| $3.65");
				System.out.println("B1| Moonpie| $1.80");
				System.out.println("B2| Cowtales| $1.50");
				System.out.println("B3| Wonka Bar| $1.50");
				System.out.println("B4| Crunchie| $1.75");
				System.out.println("C1| Cola| $1.25");
				System.out.println("C2| Dr. Salt| $1.50");
				System.out.println("C3| Mountain Melter| $1.50");
				System.out.println("C4| Heavy| $1.50");
				System.out.println("D1| U-Chews| $0.85");
				System.out.println("D2| Little League Chew| $0.95");
				System.out.println("D3| Chiclets| $0.75");
				System.out.println("D4| Triplemint| $0.75");
				System.out.println();
				System.out.println();
			}  
		
			if (choice.equals(MAIN_MENU_OPTION_PURCHASE)) {
				while (!finished) {	
					choice = (String) menu.getChoiceFromProcessOptions(PROCESS_MENU_OPTIONS, coke.getFeedMoney());
					if (choice.equals(MAIN_MENU_OPTION_FINISH_TRANSACTION)) {
						List<String> itemList = new ArrayList<String>();
						menu.printReport(coke, salesReport);
						System.out.println(coke.changeBack());
						itemList = coke.getSounds();
						for (String item : itemList) {
							System.out.println(item);
						}
						finished = true;
					} else if (choice.equals(MAIN_MENU_OPTION_FEED_MONEY)) {
						System.out.println();
						System.out.println("Please feed money in whole dollars i.e $20, $10, $5, $2, $1");
						double currentBalance = coke.getFeedMoney();
						String moneyFed = menu.getDollarAmountFromUser();
						coke.feedMoney(moneyFed);
						if (currentBalance < coke.getFeedMoney()) {
							menu.logEntry(time.toString() + "  FEED MONEY : " + " $" + moneyFed  + ".00" +
									String.format("%s%.2f", " $", coke.getFeedMoney()));
							menu.printAudit();
						}
					} else if (choice.equals(MAIN_MENU_OPTION_SELECT_PRODUCT)) {
						System.out.println();
						String key = null;
						double currentFunds = coke.getFeedMoney();
						System.out.println("Which item would you like to select?");
						key = menu.getProductKey();
						coke.selectProduct(key.toUpperCase(), coke);
						boolean isProduct = coke.isProduct(key.toUpperCase());
						int currentSold = 0;
						if (isProduct) {
							try {
								currentSold = salesReport.get(coke.getProduct(key.toUpperCase()));
							} catch (NullPointerException e) {
								//eat the exception
							}
							currentSold++;
							salesReport.put(coke.getProduct(key.toUpperCase()), currentSold);
							menu.logEntry(time.toString() + "  " + coke.productMenu.get(key.toUpperCase()) + String.format("%s%.2f", ": $", currentFunds)  
								+  String.format("%s%.2f", " $", coke.getFeedMoney()));
							menu.printAudit();
						}
					}
				}
				
			} else if (choice.equals(MAIN_MENU_OPTION_FINISH_TRANSACTION)) {
				finished = true;
			}
			
		}
	}
	
	public static void main(String[] args) {
		Menu menu = new Menu(System.in, System.out);
		VendingMachineCLI cli = new VendingMachineCLI(menu);
//		System.out.println(menu.getSalesReport());
		cli.run();
	}
}
